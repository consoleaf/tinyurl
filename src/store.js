import Vue from "vue";
import Vuex from "vuex";
import cookies from "cookiesjs";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    password: cookies("password"),
    authorized: false
  },
  mutations: {
    WRITE_PASSWORD(state, password) {
      state.password = password;
      cookies({ password: password });
    },
    WRITE_AUTHORIZED(state, authorized) {
      state.authorized = authorized;
    }
  },
  actions: {
    checkLogin() {
      fetch(
        "http://tinyurl.profi-dz.ru/admin/api/misc/login.php?password=" +
          this.state.password
      )
        .then(resp => resp.json())
        .then(json => {
          if (json.state === "unauthorized")
            this.commit("WRITE_AUTHORIZED", false);
          else this.commit("WRITE_AUTHORIZED", true);
        });
    }
  }
});
