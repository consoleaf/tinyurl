import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import store from "./store";

Vue.config.productionTip = true;
Vue.config.devtools = true;

window.app = new Vue({
  store,
  render: h => h(App)
});
window.app.$mount("#app");
