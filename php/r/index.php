<?php

require_once "../admin/api/misc/mysql.php";

$href = null;
$id = $link->real_escape_string($_GET["i"]);

$backupHref = $link->query("SELECT `value` FROM config WHERE field = 'placeholderURL'")->fetch_array()[0];

$date = (new DateTime())->format("Y-m-d");
//echo "\"SELECT origin FROM links WHERE href LIKE '%$id' AND date >= '$date'\"";
$href = $link->query("SELECT origin FROM links WHERE href LIKE '%$id' AND date >= '$date'")->fetch_assoc()["origin"];

if ($href) {
    ?>
    <script>window.location.href = '<?php echo $href; ?>'</script><?php
} else {
    ?>
    <script>window.location.href = '<?php echo $backupHref; ?>'</script><?php
}