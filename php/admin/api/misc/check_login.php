<?php
/**
 * Created by IntelliJ IDEA.
 * User: takanashi
 * Date: 22.03.19
 * Time: 19:38
 */

require_once "mysql.php";

header("Access-Control-Allow-Origin: *");

$password = $_REQUEST["password"];

$res = $link->query("SELECT `value` FROM config WHERE field = 'password' LIMIT 1");

if ($password != $res->fetch_assoc()["value"]) {
    echo json_encode(["state" => "unauthorized"]);
    exit();
}