<?php
/**
 * Created by IntelliJ IDEA.
 * User: takanashi
 * Date: 22.03.19
 * Time: 20:24
 */

require_once "misc/check_login.php";

function genHref($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return "http://tinyurl.profi-dz.ru/r?i=" . $string;
}


$origin = $link->real_escape_string($_POST["origin"]);
$date = $_POST["date"];

//error_log($date);
$date = DateTime::createFromFormat("d/m/Y", $date);
$date = $date->format("Y-m-d");

$href = genHref(8);
while ($link->query("SELECT COUNT(href) cnt FROM links WHERE href = '$href'")->fetch_assoc()["cnt"] != 0)
    $href = genHref(8);

$link->query("INSERT INTO links(href, origin, date) VALUE ('$href', '$origin', '$date')");

echo json_encode(["state" => "OK"]);