<?php
/**
 * Created by IntelliJ IDEA.
 * User: takanashi
 * Date: 22.03.19
 * Time: 19:53
 */

require_once "misc/check_login.php";

$data = [];

$config = $link->query("SELECT field, `value` FROM config");
$data["config"] = $config->fetch_all(MYSQLI_ASSOC);

$links = $link->query("SELECT href, origin, date FROM links");
$data["links"] = $links->fetch_all(MYSQLI_ASSOC);

echo json_encode(["state" => "OK", "data" => $data]);