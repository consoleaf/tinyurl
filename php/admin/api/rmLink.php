<?php
/**
 * Created by IntelliJ IDEA.
 * User: takanashi
 * Date: 22.03.19
 * Time: 22:40
 */

require_once "misc/check_login.php";

$href = $link->real_escape_string($_POST["href"]);

$link->query("DELETE FROM links WHERE href LIKE '%$href'");

echo json_encode(["state" => "OK"]);